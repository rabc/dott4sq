// Created on 26/09/2019

import UIKit
import AppServices

class VenueDetailsViewModel {
    
    private enum DetailsInformation: Int, CaseIterable {
        case address
        case distance
        case categories
    }
    
    private let venue: Venue
    
    init(venue: Venue) {
        self.venue = venue
    }
    
    func title() -> String {
        return venue.name
    }
    
    func numberOfRows() -> Int {
        return DetailsInformation.allCases.count
    }
    
    func cellViewModel(for indexPath: IndexPath) -> VenueInformationViewModel {
        guard let row = DetailsInformation(rawValue: indexPath.row) else {
            assert(false, "Wrong row")
            return VenueInformationViewModel(iconImageView: nil, title: "", text: "")
        }
        
        let viewModel: VenueInformationViewModel
        switch row {
        case .address:
            viewModel = VenueInformationViewModel(iconImageView: UIImage(named: "icon_map_pin"),
                                                  title: "address_title".localized,
                                                  text: venue.location.formattedAddress.joined(separator: " "))
        case .categories:
            viewModel = VenueInformationViewModel(iconImageView: UIImage(named: "icon_list"),
                                                  title: "categories_title".localized,
                                                  text: venue.categories.map({ $0.name }).joined(separator: ", "))
        case .distance:
            viewModel = VenueInformationViewModel(iconImageView: UIImage(named: "icon_distance"),
                                                  title: "distance_title".localized,
                                                  text: "\(venue.location.distance)m")
        }
        
        return viewModel
    }
    
}
