// Created on 27/09/2019

import UIKit

struct VenueInformationViewModel {
    let iconImageView: UIImage?
    let title: String
    let text: String
}
