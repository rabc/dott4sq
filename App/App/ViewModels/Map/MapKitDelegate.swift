// Created on 25/09/2019

import UIKit
import MapKit
import AppServices
import RxSwift

protocol MapDelegate {
    var pointSelected: Observable<Venue> { get }
    var regionChanged: Observable<CLLocationCoordinate2D> { get }
    
    init(mapView: MKMapView)
    func pinPoints(_ points: [Venue])
}


class MapKitDelegate: NSObject, MapDelegate {
    
    let clusterCellSize: CGFloat = 200
    
    private let mapView: MKMapView
    private var points = [Venue]()
    
    private let pointSelectedSubject = PublishSubject<Venue>()
    private let regionChangedSubject = PublishSubject<CLLocationCoordinate2D>()
    
    var pointSelected: Observable<Venue> { return pointSelectedSubject.asObservable() }
    var regionChanged: Observable<CLLocationCoordinate2D> { return regionChangedSubject.asObservable() }
        
    /// Initialize the delegate with the map view
    /// - Parameter mapView:
    required init(mapView: MKMapView) {
        
        self.mapView = mapView
        
        self.mapView.register(PinAnnotationView.self,
                              forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        self.mapView.register(ClusterAnnotationView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier)
        
        super.init()
        
        self.mapView.delegate = self
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didPanMap(_:)))
        panGesture.maximumNumberOfTouches = 1 // to trigger only on moving and not on zooming
        panGesture.delegate = self
        self.mapView.addGestureRecognizer(panGesture)
    }
    
    func pinPoints(_ points: [Venue]) {
        self.points = points
        
        mapView.removeAnnotations(mapView.annotations)
        
        var annotations = [MKPointAnnotation]()
        for point in points {
            let location = point.location
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: location.lat, longitude: location.lng)
            annotation.title = point.name
            
            annotations.append(annotation)
        }
        
        mapView.addAnnotations(annotations)
        mapView.showAnnotations(annotations, animated: true)
    }
    
    @objc func calloutTapped(_ sender: UITapGestureRecognizer) {
        guard let annotation = (sender.view as? MKAnnotationView)?.annotation as? MKPointAnnotation else {
            return
        }
        
        guard let title = annotation.title else {
            return
        }
        
        guard let point = points.first(where: { $0.name == title }) else {
            return
        }
        
        pointSelectedSubject.onNext(point)
    }
}

extension MapKitDelegate: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func didPanMap(_ sender: UIPanGestureRecognizer) {
        guard sender.state == .ended else {
            return
        }
        
        regionChangedSubject.onNext(mapView.centerCoordinate)
    }
}

extension MapKitDelegate: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if view is ClusterAnnotationView {
            guard let cluster = view.annotation as? MKClusterAnnotation else {
                return
            }

            mapView.showAnnotations(cluster.memberAnnotations, animated: true)
        } else if let pinView = view as? PinAnnotationView {
            guard let annotation = pinView.annotation else {
                return
            }
            mapView.selectAnnotation(annotation, animated: true)

            let gesture = UITapGestureRecognizer(target: self, action: #selector(calloutTapped(_:)))
            view.addGestureRecognizer(gesture)
        }
    }
    
}

