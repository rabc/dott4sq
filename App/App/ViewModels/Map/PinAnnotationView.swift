// Created on 25/09/2019

import MapKit

class PinAnnotationView: MKAnnotationView {
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        canShowCallout = true
        isDraggable = true
        image = UIImage(named: "icon_map_pin")
        frame = CGRect(x: 7, y: 0, width: 26, height: 36)
        
        clusteringIdentifier = "PinView"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
}
