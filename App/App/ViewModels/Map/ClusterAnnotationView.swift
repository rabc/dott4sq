// Created on 25/09/2019

import MapKit

class ClusterAnnotationView: MKAnnotationView {
    
    let colors = [UIColor.blue,
                  UIColor.gray,
                  UIColor.purple,
                  UIColor.black]
    
    let countLabel: UILabel = {
        let lbl = UILabel(frame: CGRect.zero)
        lbl.textAlignment = .center
        lbl.textColor = UIColor.white
        lbl.adjustsFontSizeToFitWidth = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        
        return lbl
    }()
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        collisionMode = .circle
        canShowCallout = false
        
        frame = CGRect(x: 0, y: 0, width: 26, height: 27)
        backgroundColor = colors[Int.random(in: 0..<colors.count)]
        self.layer.cornerRadius = frame.size.width/2
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForDisplay() {
        super.prepareForDisplay()
        
        if let cluster = annotation as? MKClusterAnnotation {
            if countLabel.superview == nil {
                self.addSubview(countLabel)
                
                // position the number label in the center
                NSLayoutConstraint.activate([countLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                                             countLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                                             countLabel.widthAnchor.constraint(lessThanOrEqualTo: self.widthAnchor, constant: -5),
                                             countLabel.heightAnchor.constraint(lessThanOrEqualTo: self.heightAnchor)])
            }
            
            countLabel.text = String(cluster.memberAnnotations.count)
        }
    }
}

