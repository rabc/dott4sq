// Created on 25/09/2019

import UIKit
import RxSwift
import AppServices
import CoreLocation

class PlacesViewModel {
    
    let mapDelegate: MapDelegate
    let services: VenuesServices
    
    private(set) var venues = [Venue]() {
        didSet {
            mapDelegate.pinPoints(venues)
        }
    }
    
    init(mapDelegate: MapDelegate, services: VenuesServices = VenuesServices()) {
        self.mapDelegate = mapDelegate
        self.services = services
    }
    
    func loadData(latitude: Double, longitude: Double) -> Completable {
        return services.search(latitude: latitude, longitude: longitude)
            .observeOn(MainScheduler.instance)
            .map { [weak self] (venues) in
                self?.venues = venues
        }.asCompletable()
    }
}



