// Created on 25/09/2019

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
