// Created on 25/09/2019

import UIKit

extension UIAlertController {
    class func singleButtonAlert(message: String, title: String = "alert_title".localized,
                                 buttonTitle: String = "OK", defaultAction: (() -> Void)? = nil) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: buttonTitle, style: .default) { (action) in
            defaultAction?()
        }
        
        alertController.addAction(OKAction)
        
        return alertController
    }
}


