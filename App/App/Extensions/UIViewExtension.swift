// Created on 25/09/2019

import UIKit

extension UIView {
    func makeEdgesEqualToSuperview() {
        guard let superview = superview else {
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([self.topAnchor.constraint(equalTo: superview.topAnchor),
                                     self.leadingAnchor.constraint(equalTo: superview.leadingAnchor),
                                     self.trailingAnchor.constraint(equalTo: superview.trailingAnchor),
                                     self.bottomAnchor.constraint(equalTo: superview.bottomAnchor)])
    }
    
    static func loadingOverlay(frame: CGRect) -> UIView {
        let overlay = UIView(frame: frame)
        overlay.backgroundColor = UIColor.clear
        
        let middleView = UIView(frame: CGRect.zero)
        middleView.translatesAutoresizingMaskIntoConstraints = false
        middleView.backgroundColor = UIColor.white
        middleView.layer.masksToBounds = false
        middleView.layer.cornerRadius = 10.0
        
        middleView.widthAnchor.constraint(equalToConstant: 80.0).isActive = true
        middleView.heightAnchor.constraint(equalToConstant: 80.0).isActive = true
        overlay.addSubview(middleView)
        
        middleView.centerXAnchor.constraint(equalTo: overlay.centerXAnchor).isActive = true
        middleView.centerYAnchor.constraint(equalTo: overlay.centerYAnchor).isActive = true
        
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.color = UIColor.gray
        activityIndicator.startAnimating()
        middleView.addSubview(activityIndicator)
        
        activityIndicator.centerXAnchor.constraint(equalTo: middleView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: middleView.centerYAnchor).isActive = true
        
        overlay.setNeedsUpdateConstraints()
        overlay.setNeedsLayout()
        
        return overlay
    }
}
