// Created on 26/09/2019

import UIKit

class VenueInformationTableViewCell: UITableViewCell, ReusableCell, NibLoadable {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var informationTextLabel: UILabel!
    
    func setup(with viewModel: VenueInformationViewModel) {
        iconImageView.image = viewModel.iconImageView
        titleLabel.text = viewModel.title
        informationTextLabel.text = viewModel.text
    }
    
}
