// Created on 26/09/2019

import UIKit

/// Every cell that needs to be reused can extend from this protocol for the setup and dequeue process
protocol ReusableCell: class {
    static var reuseIdentifier: String { get }
}

extension ReusableCell {
    /// Our default is the string of the class name
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

protocol NibLoadable: class {
    static var nib: UINib { get }
    static var nibName: String { get }
}

extension NibLoadable {
    static var nibName: String {
        return String(describing: self)
    }
    
    static var bundle: Bundle {
        return Bundle(for: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: Self.nibName, bundle: Self.bundle)
    }
}


extension UITableView {
    /// Register nib to reuse in the table view.
    /// The class must confom to `ReusableCell` and `NibLoadable` to be valid.
    ///
    /// - Parameter cell: Class to register.
    func registerNibForReuse<T: UITableViewCell>(cell: T.Type) where T: ReusableCell, T: NibLoadable {
        self.register(cell.nib, forCellReuseIdentifier: cell.reuseIdentifier)
    }
}
