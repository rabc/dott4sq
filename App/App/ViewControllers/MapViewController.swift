// Created on 25/09/2019

import UIKit
import MapKit
import RxSwift

protocol CoordinatedController: class {
    var coordinator: AppCoordinator? { get set }
}

class MapViewController: UIViewController, CoordinatedController {
    
    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        
        return manager
    }()
    
    lazy var loadingOverlay: UIView = UIView.loadingOverlay(frame: self.view.frame)
    
    let bag = DisposeBag()
    
    let mapView: MKMapView = MKMapView()
    let viewModel: PlacesViewModel
    
    weak var coordinator: AppCoordinator?
    
    required init(viewModel: PlacesViewModel? = nil) {
        if let viewModel = viewModel {
            self.viewModel = viewModel
        } else {
            let mkDelegate = MapKitDelegate(mapView: self.mapView)
            self.viewModel = PlacesViewModel(mapDelegate: mkDelegate)
        }
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "nearby_restaurants_title".localized
        
        self.view.addSubview(mapView)
        mapView.makeEdgesEqualToSuperview()
        
        subscribe()
        askForLocation()
    }
    
    func subscribe() {
        viewModel.mapDelegate
            .regionChanged
            .subscribe(onNext: self.loadData)
            .disposed(by: bag)
        
        viewModel.mapDelegate
            .pointSelected
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [coordinator] (venue) in
                coordinator?.handleScene(.details(venue: venue))
            }).disposed(by: bag)
    }
}

extension MapViewController {
    
    func loadData(from coordinate: CLLocationCoordinate2D) {
        
        if loadingOverlay.superview == nil {
            self.view.addSubview(loadingOverlay)
        }
        
        viewModel
            .loadData(latitude: coordinate.latitude, longitude: coordinate.longitude)
            .observeOn(MainScheduler.instance)
            .subscribe(onCompleted: { [weak self] in
                self?.loadingOverlay.removeFromSuperview()
            }) { [weak self] (_) in
                self?.displaySearchError()
        }.disposed(by: bag)
    }
    
    func askForLocation() {
        self.view.addSubview(loadingOverlay)
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
}

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        
        guard let location = locations.first else {
            displayLocationError()
            return
        }
        
        loadData(from: location.coordinate)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        displayLocationError()
    }
}

extension MapViewController {
    func displayLocationError() {
        loadingOverlay.removeFromSuperview()
        let alert = UIAlertController.singleButtonAlert(message: "location_error".localized)
        self.present(alert, animated: true, completion: nil)
    }
    
    func displaySearchError() {
        loadingOverlay.removeFromSuperview()
        let alert = UIAlertController.singleButtonAlert(message: "search_error".localized)
        self.present(alert, animated: true, completion: nil)
    }
}
