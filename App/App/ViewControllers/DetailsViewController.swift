// Created on 26/09/2019

import UIKit

class DetailsViewController: UIViewController {
    
    let tableView = UITableView()
    let viewModel: VenueDetailsViewModel
    
    required init(viewModel: VenueDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.title()
        
        tableView.registerNibForReuse(cell: VenueInformationTableViewCell.self)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80.0

        self.view.addSubview(tableView)
        tableView.makeEdgesEqualToSuperview()
        tableView.dataSource = self
    }
}

extension DetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VenueInformationTableViewCell.reuseIdentifier,
                                                       for: indexPath) as? VenueInformationTableViewCell else {
                                                        assert(false, "Cell must be of type VenueInformationTableViewCell")
                                                        return UITableViewCell()
        }
        
        let cellViewModel = viewModel.cellViewModel(for: indexPath)
        cell.setup(with: cellViewModel)
        
        return cell
        
    }
}
