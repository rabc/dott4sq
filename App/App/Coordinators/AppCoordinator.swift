// Created on 25/09/2019

import UIKit
import AppServices

enum AppScenes {
    case details(venue: Venue)
}

class AppCoordinator {
    let window: UIWindow
    let navController: UINavigationController
    
    init(window: UIWindow, navController: UINavigationController = UINavigationController()) {
        self.window = window
        self.navController = navController
    }
    
    func start() {
        let mapVC = MapViewController()
        mapVC.coordinator = self
        mapVC.loadViewIfNeeded()
        
        navController.viewControllers = [mapVC]
        window.rootViewController = navController
    }
    
    func handleScene(_ scene: AppScenes) {
        switch scene {
        case let .details(venue):
            let viewModel = VenueDetailsViewModel(venue: venue)
            navController.pushViewController(DetailsViewController(viewModel: viewModel), animated: true)
        }
    }
}
