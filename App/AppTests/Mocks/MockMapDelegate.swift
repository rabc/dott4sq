// Created on 26/09/2019

import Foundation
import RxSwift
import AppServices
import MapKit
@testable import App

class MockMapDelegate: MapDelegate {
    
    var pointSelected: Observable<Venue> = PublishSubject<Venue>().asObservable()
    
    private let regionChangedSubject = PublishSubject<CLLocationCoordinate2D>()
    var regionChanged: Observable<CLLocationCoordinate2D> { return regionChangedSubject.asObservable() }
    
    required init(mapView: MKMapView) {
        
    }
    
    var points: [Venue]?
    func pinPoints(_ points: [Venue]) {
        self.points = points
    }
    
    func changeRegion(to coordinate: CLLocationCoordinate2D) {
        regionChangedSubject.onNext(coordinate)
    }
}
