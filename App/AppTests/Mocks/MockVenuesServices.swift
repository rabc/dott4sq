// Created on 26/09/2019

import Foundation
import AppServices
import MapKit
import RxSwift
@testable import App

class MockVenuesServices: VenuesServices {
    
    enum MockVenuesServicesError: Error {
        case genericError
    }
    
    var latitude: Double?
    var longitude: Double?
    
    var response: [Venue]?
    override func search(latitude: Double, longitude: Double, limit: Int = 50, radius: Int = 300, type: VenuesTypes = .food) -> Single<[Venue]> {
        
        self.latitude = latitude
        self.longitude = longitude
        
        return Single<[Venue]>.create { single in
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if let response = self.response {
                    single(.success(response))
                } else {
                    single(.error(MockVenuesServicesError.genericError))
                }
            }
            
            return Disposables.create()
        }
    }
    
    static func mockWithVenues() -> MockVenuesServices {
        let mockService = MockVenuesServices()
        mockService.response = Venue.mockedVenues()
        
        return mockService
    }
}

extension Venue {
    static func mockedVenues() -> [Venue] {
        let jsonURL = Bundle(for: MockVenuesServices.self).url(forResource: "venues", withExtension: "json")!
        let data = try! Data(contentsOf: jsonURL)
        let venues = try! JSONDecoder().decode([Venue].self, from: data)
        
        return venues
    }
}
