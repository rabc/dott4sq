// Created on 26/09/2019

import XCTest
import AppServices
import MapKit
@testable import App

class MockMapView: MKMapView {
    
    var newCenterCoordinate: CLLocationCoordinate2D?
    override var centerCoordinate: CLLocationCoordinate2D {
        get { return newCenterCoordinate ?? super.centerCoordinate }
        set { }
    }
    
    var addedAnnotations = [MKPointAnnotation]()
    override func addAnnotations(_ annotations: [MKAnnotation]) {
        if let anns = annotations as? [MKPointAnnotation] {
            addedAnnotations = anns
        }
    }
    
    var calledShowAnnotations: Bool = false
    var displayedAnnotations: [MKAnnotation]?
    override func showAnnotations(_ annotations: [MKAnnotation], animated: Bool) {
        calledShowAnnotations = true
        displayedAnnotations = annotations
    }
    
    var selectedAnnotation: MKAnnotation?
    override func selectAnnotation(_ annotation: MKAnnotation, animated: Bool) {
        selectedAnnotation = annotation
    }
    
    var calledRemoveAnnotations: Bool = false
    override func removeAnnotations(_ annotations: [MKAnnotation]) {
        calledRemoveAnnotations = true
    }
}

class MapKitDelegateTests: XCTestCase {

    var mapView: MockMapView!
    
    override func setUp() {
        super.setUp()
        
        mapView = MockMapView()
    }
    
    func testInit() {
        _ = MapKitDelegate(mapView: mapView)
        
        let pinView = mapView.dequeueReusableAnnotationView(withIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        let clusterView = mapView.dequeueReusableAnnotationView(withIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier)
        
        XCTAssertTrue(pinView is PinAnnotationView)
        XCTAssertTrue(clusterView is ClusterAnnotationView)
    }
    
    func testPinPoints() {
        let venues = Array(Venue.mockedVenues().prefix(upTo: 5))
        
        let delegate = MapKitDelegate(mapView: mapView)
        delegate.pinPoints(venues)
        
        XCTAssertEqual(mapView.addedAnnotations.count, venues.count)
        XCTAssertTrue(mapView.calledShowAnnotations)
        XCTAssertTrue(mapView.calledRemoveAnnotations)
        
        for annotation in mapView.addedAnnotations {
            if let venue = venues.first(where: { $0.name == annotation.title }){
                
                XCTAssertEqual(annotation.coordinate.latitude, venue.location.lat)
                XCTAssertEqual(annotation.coordinate.longitude, venue.location.lng)
                
            } else {
                XCTFail("Venue from annotation not found")
            }
        }
    }
    
    func testCalloutTapped() {
        let venues = Venue.mockedVenues()
        
        let delegate = MapKitDelegate(mapView: mapView)
        delegate.pinPoints(venues)
        
        let annotation = MKPointAnnotation()
        annotation.title = venues.first!.name
        
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
        
        let gesture = MockTap()
        gesture.annotationView = annotationView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            delegate.calloutTapped(gesture)
        }
        
        let point = try! delegate.pointSelected.toBlocking().first()
        XCTAssertEqual(point, venues.first!)
    }
    
    func testDidSelectClusterView() {
     
        let venues = Venue.mockedVenues()
        let delegate = MapKitDelegate(mapView: mapView)
        
        let annotation = MKPointAnnotation()
        annotation.title = venues.first!.name
        
        let clusterView = ClusterAnnotationView(annotation: MKClusterAnnotation(memberAnnotations: [annotation]), reuseIdentifier: nil)
        delegate.mapView(mapView, didSelect: clusterView)
        
        XCTAssertEqual(mapView.displayedAnnotations?.count, 1)
        XCTAssertEqual(mapView.displayedAnnotations?.first?.title, annotation.title)
    }
    
    func testDidSelectPinView() {
     
        let venues = Venue.mockedVenues()
        let delegate = MapKitDelegate(mapView: mapView)
        
        let annotation = MKPointAnnotation()
        annotation.title = venues.first!.name
        
        let pinView = PinAnnotationView(annotation: annotation, reuseIdentifier: nil)
        delegate.mapView(mapView, didSelect: pinView)
        
        XCTAssertEqual((mapView.selectedAnnotation as? MKPointAnnotation)?.title, annotation.title)
        XCTAssertTrue(pinView.gestureRecognizers?.first is UITapGestureRecognizer)
    }

    func testRegionChange() {
        let mockMapView = MockMapView()
        mockMapView.newCenterCoordinate = CLLocationCoordinate2D(latitude: 99, longitude: 99)
        let delegate = MapKitDelegate(mapView: mockMapView)
        
        let panGesture = UIPanGestureRecognizer()
        panGesture.state = .ended
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            delegate.didPanMap(panGesture)
        }
        
        let coordinate = try! delegate.regionChanged.toBlocking().first()
        XCTAssertEqual(coordinate?.latitude, mockMapView.newCenterCoordinate!.latitude)
        XCTAssertEqual(coordinate?.longitude, mockMapView.newCenterCoordinate!.longitude)
        
    }
    
}

class MockTap: UITapGestureRecognizer {
    var annotationView: MKAnnotationView?
    override var view: UIView? { return annotationView }
}

