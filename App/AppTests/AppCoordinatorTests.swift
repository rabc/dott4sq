// Created on 26/09/2019

import XCTest
import AppServices
@testable import App

class MockNavigationController: UINavigationController {
    var pushedViewController: UIViewController?
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
    }
}

class AppCoordinatorTests: XCTestCase {
    
    var mockedNavController: MockNavigationController!
    
    override func setUp() {
        super.setUp()
        mockedNavController = MockNavigationController()
    }

    func testStart() {
        let window = UIWindow()
        let coordinator = AppCoordinator(window: window, navController: mockedNavController)
        coordinator.start()
        
        XCTAssertTrue(mockedNavController.topViewController is MapViewController)
    }
    
    func testHandleScene() {
        let window = UIWindow()
        let coordinator = AppCoordinator(window: window, navController: mockedNavController)
        
        let venue = Venue.mockedVenues().first!
        coordinator.handleScene(.details(venue: venue))
        
        if let detailsViewController = mockedNavController.pushedViewController as? DetailsViewController {
            XCTAssertEqual(detailsViewController.viewModel.title(), venue.name)
        } else {
            XCTFail("Should push DetailsViewController")
        }
        
    }

}
