// Created on 27/09/2019

import XCTest
@testable import App

class VenueInformationTableViewCellTests: XCTestCase {
    func testSetup() {
        let vm = VenueInformationViewModel(iconImageView: nil, title: "My Title", text: "My Text")
        let cell = VenueInformationTableViewCell.bundle
                        .loadNibNamed(VenueInformationTableViewCell.nibName, owner: nil)?.first as! VenueInformationTableViewCell
        cell.setup(with: vm)
        
        XCTAssertNil(cell.iconImageView.image)
        XCTAssertEqual(cell.titleLabel.text, vm.title)
        XCTAssertEqual(cell.informationTextLabel.text, vm.text)
    }
}
