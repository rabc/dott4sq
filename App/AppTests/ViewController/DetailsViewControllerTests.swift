// Created on 27/09/2019

import XCTest
import AppServices
@testable import App

class MockVenueDetailsViewModel: VenueDetailsViewModel {
    var calledNumberOfRows: Bool = false
    override func numberOfRows() -> Int {
        calledNumberOfRows = true
        return 1
    }
    
    var indexPath: IndexPath?
    override func cellViewModel(for indexPath: IndexPath) -> VenueInformationViewModel {
        self.indexPath = indexPath
        return VenueInformationViewModel(iconImageView: nil, title: "My Title", text: "My Text")
    }
}

class DetailsViewControllerTests: XCTestCase {

    func testTableViewDataSourceFunctions() {
        let viewModel = MockVenueDetailsViewModel(venue: Venue.mockedVenues().first!)
        let detailsViewController = DetailsViewController(viewModel: viewModel)
        detailsViewController.viewDidLoad()
        
        let indexPath = IndexPath(row: 99, section: 99)
        
        let rows = detailsViewController.tableView(detailsViewController.tableView, numberOfRowsInSection: indexPath.section)
        XCTAssertEqual(rows, viewModel.numberOfRows())
        XCTAssertTrue(viewModel.calledNumberOfRows)
        
        if let cell = detailsViewController.tableView(detailsViewController.tableView, cellForRowAt: indexPath) as? VenueInformationTableViewCell {
            XCTAssertEqual(viewModel.indexPath, indexPath)
            XCTAssertNil(cell.iconImageView.image)
            XCTAssertEqual(cell.titleLabel.text, "My Title")
            XCTAssertEqual(cell.informationTextLabel.text, "My Text")
        } else {
            XCTFail("Cell should be of type VenueInformationTableViewCell")
        }
        
    }

}
