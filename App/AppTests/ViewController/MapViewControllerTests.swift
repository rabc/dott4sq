// Created on 26/09/2019

import XCTest
import MapKit
@testable import App

class MapViewControllerTests: XCTestCase {

    func testLocationUpdate() {
        let mockDelegate = MockMapDelegate(mapView: MKMapView())
        let mockService = MockVenuesServices.mockWithVenues()
        let viewModel = PlacesViewModel(mapDelegate: mockDelegate, services: mockService)
        
        let mapVC = MapViewController(viewModel: viewModel)
        mapVC.viewDidLoad()
        let location = CLLocation(latitude: 1, longitude: 1)
        
        mapVC.locationManager.stopUpdatingLocation()
        mapVC.locationManager(mapVC.locationManager, didUpdateLocations: [location])
        
        let exp = expectation(description: "waitForLoad")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 3) { (error) in
            XCTAssertNil(error)
            XCTAssertEqual(mockService.latitude, 1)
            XCTAssertEqual(mockService.longitude, 1)
            XCTAssertEqual(mockDelegate.points, mockService.response!)
        }
    }
    
    func testLocationChange() {
        let mockDelegate = MockMapDelegate(mapView: MKMapView())
        let mockService = MockVenuesServices.mockWithVenues()
        let viewModel = PlacesViewModel(mapDelegate: mockDelegate, services: mockService)
        
        let mapVC = MapViewController(viewModel: viewModel)
        mapVC.viewDidLoad()
        mapVC.locationManager.stopUpdatingLocation()
        
        let location = CLLocationCoordinate2D(latitude: 2, longitude: 2)
        mockDelegate.changeRegion(to: location)
        
        let exp = expectation(description: "waitForLoad")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 3) { (error) in
            XCTAssertNil(error)
            XCTAssertEqual(mockService.latitude, 2)
            XCTAssertEqual(mockService.longitude, 2)
        }
        
    }

}
