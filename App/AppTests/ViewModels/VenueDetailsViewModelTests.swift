// Created on 27/09/2019

import XCTest
import AppServices
import RxSwift
import RxBlocking
@testable import App

class VenueDetailsViewModelTests: XCTestCase {
    
    let venue = Venue.mockedVenues().first!
    
    func testTitle() {
        let vm = VenueDetailsViewModel(venue: venue)
        XCTAssertEqual(vm.title(), venue.name)
    }
    
    func testNumberOfRows() {
        let vm = VenueDetailsViewModel(venue: venue)
        XCTAssertEqual(vm.numberOfRows(), 3)
    }
    
    func testCellViewModel_Address() {
        let vm = VenueDetailsViewModel(venue: venue)
        let cellVM = vm.cellViewModel(for: IndexPath(row: 0, section: 0))
        XCTAssertEqual(cellVM.iconImageView, UIImage(named: "icon_map_pin"))
        XCTAssertEqual(cellVM.text, venue.location.formattedAddress.joined(separator: " "))
        XCTAssertEqual(cellVM.title, "address_title".localized)
    }
    
    func testCellViewModel_Categories() {
        let vm = VenueDetailsViewModel(venue: venue)
        let cellVM = vm.cellViewModel(for: IndexPath(row: 2, section: 0))
        XCTAssertEqual(cellVM.iconImageView, UIImage(named: "icon_list"))
        XCTAssertEqual(cellVM.text, venue.categories.map({ $0.name }).joined(separator: ", "))
        XCTAssertEqual(cellVM.title, "categories_title".localized)
    }
    
    func testCellViewModel_Distance() {
        let vm = VenueDetailsViewModel(venue: venue)
        let cellVM = vm.cellViewModel(for: IndexPath(row: 1, section: 0))
        XCTAssertEqual(cellVM.iconImageView, UIImage(named: "icon_distance"))
        XCTAssertEqual(cellVM.text, "\(venue.location.distance)m")
        XCTAssertEqual(cellVM.title, "distance_title".localized)
    }

}
