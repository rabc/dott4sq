// Created on 25/09/2019

import XCTest
import AppServices
import MapKit
import RxSwift
import RxBlocking
@testable import App

class PlacesViewModelTests: XCTestCase {

    func testLoadData() {
        let mockDelegate = MockMapDelegate(mapView: MKMapView())
        let mockService = MockVenuesServices.mockWithVenues()
        
        let viewModel = PlacesViewModel(mapDelegate: mockDelegate, services: mockService)
        _ = try! viewModel.loadData(latitude: 1, longitude: 1).toBlocking().first()
  
        XCTAssertEqual(mockService.latitude, 1)
        XCTAssertEqual(mockService.longitude, 1)
        XCTAssertEqual(mockDelegate.points, mockService.response!)
    }

}

extension Venue: Equatable {
    public static func == (lhs: Venue, rhs: Venue) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
}

