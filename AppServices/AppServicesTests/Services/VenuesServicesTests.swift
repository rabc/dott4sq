// Created on 25/09/2019

import XCTest
import RxSwift
import RxBlocking
@testable import AppServices

class MockRequestManager: RequestManager {
    
    enum MockRequestManagerError: Error {
        case genericError
    }
    
    var response: Decodable?
    
    var route: RouterManagerConvertible?
    var parameters: [QueryConvertible]?
    override func make<T: Decodable>(route: RouterManagerConvertible, parameters: [QueryConvertible] = []) -> Single<T> {
        self.route = route
        self.parameters = parameters
        
        if let response = response {
            return Single.just(response as! T)
        }
        
        return Single.error(MockRequestManagerError.genericError)
    }
}

class VenuesServicesTests: XCTestCase {

    func testSearch() {
        let location = VenueLocation(address: "", lat: 1, lng: 1, distance: 1, formattedAddress: [])
        let venue = Venue(id: "123", name: "My Restaurant", location: location, categories: [])
        let response = VenuesSearch(venues: [venue])
        
        let params = VenueSearchParameters(lat: 1, lng: 1, limit: 1, radius: 1, categoryId: VenuesTypes.food.rawValue)
        
        let manager = MockRequestManager()
        manager.response = response
        
        let service = VenuesServices(request: manager)
        
        let result = try! service.search(latitude: params.lat, longitude: params.lng,
                                         limit: params.limit, radius: params.radius, type: .food).toBlocking().first()
        
        XCTAssertEqual(result?.count, 1)
        XCTAssertEqual(result?.first, venue)
        XCTAssertEqual(manager.parameters?.first?.queryItems(), params.queryItems())
        
        if let route = manager.route as? VenuesServicesRouter {
            XCTAssertEqual(route, VenuesServicesRouter.search)
        } else {
            XCTFail("Wrong route")
        }
    }
    
    func testVenueSearchParameters() {
        let params = VenueSearchParameters(lat: 1, lng: 1, limit: 1, radius: 1, categoryId: VenuesTypes.food.rawValue)
        let queryItems = params.queryItems()
        
        XCTAssertEqual(queryItems.first(where: { $0.name == "ll" })?.value, "1.0,1.0")
        XCTAssertEqual(queryItems.first(where: { $0.name == "intent" })?.value, "browse")
        XCTAssertEqual(queryItems.first(where: { $0.name == "categoryId" })?.value, VenuesTypes.food.rawValue)
        XCTAssertEqual(queryItems.first(where: { $0.name == "limit" })?.value, "1")
        XCTAssertEqual(queryItems.first(where: { $0.name == "radius" })?.value, "1")
        XCTAssertEqual(queryItems.first(where: { $0.name == "v" })?.value, KeysManager.version)
        XCTAssertEqual(queryItems.first(where: { $0.name == "client_id" })?.value, KeysManager.client_id)
        XCTAssertEqual(queryItems.first(where: { $0.name == "client_secret" })?.value, KeysManager.client_secret)
    }
    
    func testRouter() {
        let route = VenuesServicesRouter.search.asRouter()
        XCTAssertEqual(route.endpoint, "/venues/search")
        XCTAssertEqual(route.method, .get)
    }
}

extension Venue: Equatable {
    public static func == (lhs: Venue, rhs: Venue) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
}
