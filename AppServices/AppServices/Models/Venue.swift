// Created on 25/09/2019

import Foundation

public struct VenueCategory: Decodable {
    public let id: String
    public let name: String
}

public struct VenueLocation: Decodable {
    public let address: String?
    public let lat: Double
    public let lng: Double
    public let distance: Int
    public let formattedAddress: [String]
}

public struct Venue: Decodable {
    public let id: String
    public let name: String
    public let location: VenueLocation
    public let categories: [VenueCategory]
}

struct VenuesSearch: Decodable {
    
    let venues: [Venue]
    
    enum ResponseKey: CodingKey {
        case response
    }
    
    enum VenuesKey: CodingKey {
        case venues
    }
    
    init(from decoder: Decoder) throws {
        
        let mainContainer = try decoder.container(keyedBy: ResponseKey.self)
        let responseContainer = try mainContainer.nestedContainer(keyedBy: VenuesKey.self, forKey: .response)
        
        self.venues = try responseContainer.decode([Venue].self, forKey: .venues)
    }
    
    init(venues: [Venue]) {
        self.venues = venues
    }
}
