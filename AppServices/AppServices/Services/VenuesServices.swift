// Created on 25/09/2019

import Foundation
import RxSwift

struct VenueSearchParameters: QueryConvertible {
    let lat: Double
    let lng: Double
    let limit: Int
    let radius: Int
    var intent: String = "browse"
    let categoryId: String
    
    func queryItems() -> [URLQueryItem] {
        let items: [URLQueryItem] = [
            URLQueryItem(name: "ll", value: "\(lat),\(lng)"),
            URLQueryItem(name: "intent", value: intent),
            URLQueryItem(name: "categoryId", value: categoryId),
            URLQueryItem(name: "limit", value: String(limit)),
            URLQueryItem(name: "radius", value: String(radius)),
            URLQueryItem(name: "v", value: KeysManager.version),
            URLQueryItem(name: "client_id", value: KeysManager.client_id),
            URLQueryItem(name: "client_secret", value: KeysManager.client_secret)
        ]
        
        return items
    }
}

enum VenuesServicesRouter: RouterManagerConvertible {
    case search
    
    func asRouter() -> RouterManager {
        let endpoint = "/venues/search"
        let method: HTTPMethod = .get
        
        return RouterManager(endpoint: endpoint, method: method)
    }
}

public enum VenuesTypes: String {
    case food = "4d4b7105d754a06374d81259"
    case event = "4d4b7105d754a06373d81259"
}

open class VenuesServices {
    let request: RequestManager
    
    public init() {
        self.request = RequestManager.shared
    }
    
    init(request: RequestManager) {
        self.request = request
    }
    
    /// Search nearby points by venue type
    /// - Parameter latitude:
    /// - Parameter longitude:
    /// - Parameter limit: defaults to 50
    /// - Parameter radius: defaults to 300
    /// - Parameter type: defaults to `food`
    open func search(latitude: Double, longitude: Double, limit: Int = 50,
                     radius: Int = 300, type: VenuesTypes = .food) -> Single<[Venue]> {
        
        let params = VenueSearchParameters(lat: latitude, lng: longitude, limit: limit, radius: radius, categoryId: type.rawValue)
        let response: Single<VenuesSearch> = request.make(route: VenuesServicesRouter.search, parameters: [params])
        
        return response.map { $0.venues }
    }
}

