// Created on 25/09/2019

import Foundation

protocol RouterManagerConvertible {
    func asRouter() -> RouterManager
}

protocol QueryConvertible {
    func queryItems() -> [URLQueryItem]
}
