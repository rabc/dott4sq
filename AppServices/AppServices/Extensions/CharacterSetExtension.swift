// Created on 25/09/2019

import Foundation

extension CharacterSet {
    static var urlPercentEncoding: CharacterSet {
        var urlCharsAllowed = CharacterSet.urlHostAllowed
        urlCharsAllowed.subtract(CharacterSet(charactersIn: "+"))
        
        return urlCharsAllowed
    }
}
