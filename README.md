### About the project

The project is using Carthage, to build the frameworks you should run: `carthage bootstrap --no-use-binaries --platform iOS`

The workspace has 2 projects:

* `AppServices` - A framework responsible to call the API endpoints (only venues search in this case).
* `App` - The target to run the app.

I have divided into two projects with the idea of a modularized architecture, something I've been exploring more and more in recent projects (in current company and in personal projects). The `AppServices` framework exposes only the class to make the call to the API and the needed models.

I am using my own Foursquare keys, if you need to change you will find it in `KeysManager`.

In `App`, I included a protocol called `ReusableCell` to make it easier to register and reuse an `UITableViewCell`. It is a protocol I've put together from my ideas and from Swift articles on the web and I am using in personal projects.

The `MKMapView` delegate has its own class, `MapKitDelegate`, to split the responsability and avoid massive classes, also helping with testability.

The (vectorized) assets has files for both Light and Dark modes, so the app will work either way in iOS 13.

All the UI is made by code, with the exception of `VenueInformationTableViewCell` because it has more objects and to demonstrate knowledge of XIB interfaces.